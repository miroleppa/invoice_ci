package fi.vamk.e1701577;

import java.util.regex.Pattern;

public class ReferenceValidator {

    private String Invoice;
    private String Reference;
    private int ReferenceNumber;
    private final int[] Multiplier = {7, 3, 1};

    public ReferenceValidator(String invoice) {
        this.Invoice = invoice;
        this.ReferenceNumber = calculateReference();
        this.Reference = Invoice + ReferenceNumber;
    }

    /**
     * @return int between 0 and 9, for the reference number value
     */
    public int calculateReference() {
        // Reversing the input string and looking this way is way simpler
        // at least regarding the for-loop and the substring method
        String tempstr = new StringBuilder(Invoice).reverse().toString();
        int temp;
        int sum = 0;
        for (int i = 0; i < tempstr.length(); i++) {
            temp = Integer.parseInt(tempstr.substring(i, i + 1));
            sum += Multiplier[i % 3] * temp;
        }

        // get next 10-digit value
        if (sum % 10 != 0) {
            temp = sum + (10 - sum % 10);
        }   else {
            return 0;
        }
        return temp - sum;
    }
    
    /**
     * Modifies the final reference number to have spaces between every 5 letters
     * counting from the right side
     * Example: input: 12345678901 -> output: 1 23456 78901 
     * @return 
     */
    public String addSpacesToReference() {
        String temp = this.Reference;
        temp = new StringBuilder(temp).reverse().toString();
        
        String result = "";

        for (int i = 0; i < temp.length(); i++) {

            if ((i % 5 == 0) && (i != 0)) {
                result += " ";
            }
            result += temp.charAt(i);
        }

        return new StringBuilder(result).reverse().toString();
    }

    // getters

    public String getReferenceNumber() {
        return addSpacesToReference();
    }

    public String getReferenceNumberNoSpaces() {
        return Reference;
    }

    // probably could've had a toString printing the final result here too but w/e



    // I had also made proper validation methods for the input,
    // but since I'm not sure how to integrate them into the program,
    // i'm just going to let them chill here for now



    public boolean validateInputContent() {
        Pattern p = Pattern.compile("\\d*");
        return p.matcher(this.Invoice).matches();
    }

    //Create a Java-class ReferenceValidator.java that has a method to check 
    //a. min length 4 numbers (like 1232) (three numbers from invoice + plus check number)
    //b. max length 19 numbers (like 1234567890123456787)

    public boolean validateInputLength() {
        return (this.Invoice.length() > 2 && this.Invoice.length() < 20);
    }
    
}