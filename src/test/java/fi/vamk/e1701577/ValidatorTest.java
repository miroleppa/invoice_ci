package fi.vamk.e1701577;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.accessibility.AccessibleAttributeSequence;

import org.junit.Test;
import java.util.regex.Pattern;

public class ValidatorTest {
    private ReferenceValidator validator;

    
    // 5. Create a test case to check that the length of the reference number is >3 
    @Test
    public void referenceLengthAboveThree() {
        // this is probably not the proper way to do this, but
        // I CBA to make actual properties and factories for such a project
        validator = new ReferenceValidator("123");
        assertTrue(validator.getReferenceNumber().length() > 3);
    }

    // 6. Create a test case to check that the length of the reference number is <20
    @Test
    public void referenceLengthBelowTwenty() {
        validator = new ReferenceValidator("123456789012345678");
        assertTrue(validator.getReferenceNumberNoSpaces().length() < 20);
    }

    // 7. Create a test case to check that the numbers have been grouped in the groups of five numbers starting from right
    @Test
    public void referenceIsOfCorrectFormat() {
        validator = new ReferenceValidator("1234567890");
        assertEquals("1 23456 78907", validator.getReferenceNumber());
    }

    // 8. Create a test case to check that reference number only has numbers and spaces 
    @Test
    public void referenceOnlyContainsNumbersAndSpaces() {
        validator = new ReferenceValidator("1234543210");
        // some regex magic i came up by myself using the help of a regex tester website
        Pattern p = Pattern.compile("[\\s\\d]*");
        assertTrue(p.matcher(this.validator.getReferenceNumber()).matches());
    }

    // 9. Create a test case to test that the actual checking number is correct
    @Test
    public void CheckingNumberIsCorrect() {
        validator = new ReferenceValidator("234982345");
        assertEquals(4, validator.calculateReference());
    }
}