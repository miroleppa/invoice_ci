package fi.vamk.e1701577;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("The actual application doesn't really do anything");
        System.out.println("The meat of this program is in the tests and ReferenceValidator.java");
        System.out.println("This app only really works with the command 'mvn test'");
        System.out.println("In hindsight this probably could've been a library or something");
    }

}
